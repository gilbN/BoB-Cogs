import discord
from discord.ext import commands
import requests
import urllib.parse
from bs4 import BeautifulSoup


class searcher:

    def ramblings(search):
        searched = "https://technicalramblings.com/?s=" + urllib.parse.quote_plus(search)
        page = requests.get(searched)
        soup = BeautifulSoup(page.text, 'html.parser')
        post_list = soup.find(class_='page-contents')
        post_list_items = post_list.find_all("article")

        ID = 1
        results = {}

        for posts in post_list_items:
            titles = posts.find('h1')
            imgurl = posts.find("img")
            img = imgurl['src']
            title = titles.find("a").getText()
            if title is None:
                pass
            style = posts.find("a", {"class": "post-category"})
            if style is not None:
                color = style["data-category-color"]
            else:
                color = "#000000"
            links = titles.find("a")["href"]
            result = {"Title": title, "Links": links, "Color": color, "img": img}
            results[ID] = result
            ID += 1
        return results


class TechnicalSearcher:
    """Searches TechnicalRamblings.com and returns result"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, no_pm=True)
    async def techsearch(self, ctx, *, search_word):
        trigger_text = search_word
        """Searches TechnicalRamblings.com and returns result"""

        author = ctx.message.author

        embed = discord.Embed(colour=author.colour)

        em_name = ("Search triggerd by %s for: %s" % (author.name, trigger_text))

        embed.set_author(name="BoB presents:", icon_url=author.avatar_url)

        msg = ""

        try:
            output = searcher.ramblings(trigger_text)
            max_int = max(output)
            for ID in range(1, max_int + 1):
                title = output[ID]["Title"]
                link = output[ID]["Links"]
                replace = "**{0}**. [{1}]({2})\n".format(ID, title, link)
                msg += replace
                if ID == 5:
                    break
        except:
            msg = "No results found"

        payload = msg
        embed.add_field(name=em_name, value=payload)

        await self.bot.say(embed=embed)


def setup(bot):
    n = TechnicalSearcher(bot)
    bot.add_cog(n)