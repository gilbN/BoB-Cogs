import re
from discord.ext import commands


class CleanUp:
    def clean(self,full):
        if full.startswith('```'):
            full = full[3:]
        if full.endswith('```'):
            full = full[:-3]
        full = re.sub(r'((?:\w|[\-0-9])+\.(?:[a-z]{2,3})+);?$', "example.com", full)
        return full


class Lang:
    def detect(self, full, rebug):
        rebug = rebug
        treshold = 20
        bugs = "Welcome to debugmode £user, this is probably worse than the script itself\n"
        dirt = full
        code = CleanUp.clean(self, dirt)
        lines = []  # Declare an empty list named "lines"
        match = 0
        if code.startswith('nginx'):
            code = code.replace("nginx", "")
            output = "Code submited by £user\n```Nginx\n%s\n```" % (code)
            return output
        if code.startswith('css'):
            code = code.replace("css", "")
            output = "Code submited by £user\n```CSS\n%s\n```" % (code)
            return output
        if code.startswith('bash'):
            code = code.replace("bash", "")
            output = "Code submited by £user\n```Bash\n%s\n```" % (code)
            return output
        with open('data/recode/directives.txt', 'rt') as in_file:  # Open file lorem.txt for reading of text data.
            # print(in_file)
            for line in in_file:  # For each line of text in in_file, where the data is named "line",
                lines.append(line.rstrip('\n'))  # add that line to our list of lines.
                # print (lines)
                for element in lines:  # For each element in our list,
                    lines = lines
                for word in code.split():
                    if element == word:
                        match = match + 1
                        bugs = "%s\nMatched with word: %s, Match number:%s" % (bugs, word, match)
        count_code = len(code.split())
        quick_matts = 100.0 * match/count_code
        bugs ="%s\n\nWords in code: **%s** \nWords matched: **%s** \n\nCurrent treshold **%s** %%\n **%s** %% match\n\n" % (bugs, count_code, match, treshold, quick_matts)
        if rebug == "1":
            if quick_matts >= treshold:
                bugs = "%s\nCode **will** be pasted" % (bugs)
            else:
                bugs = "%s\nCode will **NOT** be pasted" % (bugs)
            bugs = "%s\nDebug done £user" % (bugs)
            return bugs
        if quick_matts >= treshold:
            output = "Code submited by £user\n```Nginx\n%s\n```" % (code)
            return output
        else:
            return "£user no language detected, defaulted to nginx\n```Nginx\n%s\n```" % (code)

class MultiCodeReply:
    """Repeats code in quotes"""

    def __init__(self, bot):
        self.bot = bot

    @commands.group(name="recode", pass_context=True, no_pm=True, invoke_without_command=True)
    async def recode(self, ctx, *, full):
        """Returns text in nginx style format"""
        auth = ctx.message.author
        output = Lang.detect(self, full,rebug="0").replace("£user", auth.mention)
        await self.bot.delete_message(ctx.message)
        await self.bot.say(output)

    @commands.command(pass_context=True, no_pm=True)
    async def rebug(self,ctx, *, full):
        """Returns debug info"""
        auth = ctx.message.author
        output = Lang.detect(full,rebug="1").replace("£user", auth.mention)
        await self.bot.delete_message(ctx.message)
        await self.bot.say(output)


def setup(bot):
    n = MultiCodeReply(bot)
    bot.add_cog(n)